#ifndef FILE_IO_H
#define FILE_IO_H

#include <string>
using std::string;
#include <vector>



class CFileIO
{
public:
    CFileIO();
    ~CFileIO();

    int read(const string &proto, int argFrame);
    static bool exist(const string &proto, int argFrame);
    int getRow();
    int getCol();
    float* getRGB();
    int getFrame();
    unsigned char* getAlpha();

private:
    int frame;
    int nPix;
    int nAlpha;

    int frameSize;

    unsigned char *inputAlloc;
    unsigned char *inputAlign;
    float *fpRGB;
    unsigned char *ucpAlpha;

    bool getByteSwap(unsigned char *header);
    int getHeaderSize(unsigned char *header);
    int getFileSize(unsigned char *header);
    int getRowCount(unsigned char *header);
    int getColCount(unsigned char *header);
    int getImageFormat(unsigned char *header);

    int unpack();
};

#endif

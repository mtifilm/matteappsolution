#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <rpc.h>
#include <pch.h>

#include "FileIO.h"


/* Constants to describe the type of source image data */
#define IMAGE_FORMAT_NONE           0
#define IMAGE_FORMAT_16BIT_Y        1
#define IMAGE_FORMAT_10BIT_RGB_L    2   /* 3 ten bit values packed to the left in 4 bytes */
#define IMAGE_FORMAT_16BIT_RGB      3   /* 3 sixteen bit values */
#define IMAGE_FORMAT_10BIT_Y_L      4   /* 3 ten bit values packed to the left in 4 bytes */
#define IMAGE_FORMAT_16BIT_RGBA     5   /* 4 sixteen bit values */


#define MAX_HEADER_SIZE 65536

CFileIO::CFileIO()
{
    inputAlloc = 0;
    inputAlign = 0;
    fpRGB = 0;
    frameSize = 0;
    ucpAlpha = 0;
    nPix = 0;
    nAlpha = 0;
}

CFileIO::~CFileIO()
{
    free(inputAlloc);
    inputAlloc = 0;
    inputAlign = 0;
    free(fpRGB);
    fpRGB = 0;
    free(ucpAlpha);
    ucpAlpha = 0;
    nPix = 0;
    nAlpha = 0;
}

bool CFileIO::exist(const string &proto, int argFrame)
{
    struct stat results;
    char caFileName[1024];
    sprintf_s(caFileName, 1024, proto.c_str(), argFrame);

    if (stat(caFileName, &results) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int CFileIO::read(const string &proto, int argFrame)
{
    struct stat results;
    char caFileName[1024];
    sprintf_s(caFileName, 1024, proto.c_str(), argFrame);

    int frameSizeLocal;

    if (stat(caFileName, &results) == 0)
    {
        // The size of the file in bytes is in
        // results.st_size
        frameSizeLocal = results.st_size;
    }
    else
    {
        // An error occurred
        return -1;
    }

    if (frameSizeLocal > frameSize)
    {
        free(inputAlloc);
        inputAlloc = 0;

        // additional storage for the largest possible header
        frameSize = frameSizeLocal + MAX_HEADER_SIZE;

        // allocate storage for the IO buffers
        inputAlloc = (unsigned char *)malloc(frameSize + 4095);
        if (inputAlloc == 0)
        {
            return -1;
        }

        inputAlign = (unsigned char *)(((uintptr_t)inputAlloc + 4095) & ~4095);
    }

    // open the file
    errno_t err;
    FILE *fp;

    err = fopen_s(&fp, caFileName, "rb");
    if (err != 0)
    {
        return -1;
    }

    if (fread(inputAlign, 1, frameSizeLocal, fp) != frameSizeLocal)
    {
        fclose(fp);
        return -1;
    }

    fclose(fp);

    unpack();

    frame = argFrame;

    return 0;
}

int CFileIO::getRow()
{
    return getRowCount(inputAlign);
}

int CFileIO::getCol()
{
    return getColCount(inputAlign);
}

float *CFileIO::getRGB()
{
    return fpRGB;
}

int CFileIO::getFrame()
{
    return frame;
}

unsigned char *CFileIO::getAlpha()
{
    return ucpAlpha;
}

int CFileIO::unpack()
{
    // get information about the image
    bool byteSwap = getByteSwap(inputAlign);
    int rowCount = getRowCount(inputAlign);
    int colCount = getColCount(inputAlign);
    int headerSize = getHeaderSize(inputAlign);
    int imageFormat = getImageFormat(inputAlign);

    if (rowCount*colCount > nPix)
    {
        free(fpRGB);
        fpRGB = 0;
        nPix = rowCount * colCount;
        fpRGB = (float *)malloc(nPix * 3 * 4);
        if (fpRGB == 0)
        {
            return -1;
        }
    }

    if (imageFormat == IMAGE_FORMAT_16BIT_RGBA)
    {
        // this file has an alpha channel
        if (rowCount*colCount > nAlpha)
        {
            free(ucpAlpha);
            ucpAlpha = 0;
            nAlpha = rowCount * colCount;
            ucpAlpha = (unsigned char *)malloc(nAlpha);
            if (ucpAlpha == 0)
            {
                return -1;
            }
        }
    }

    if (headerSize > MAX_HEADER_SIZE)
    {
        return -1;
    }

    // skip over the header
    unsigned char *input = inputAlign + headerSize;

    // unpack the data into RGBRGBRGB format
    float *fp = fpRGB;

    unsigned short usR, usG, usB, usY, usaY[3];
    int maxData;

    if (imageFormat == IMAGE_FORMAT_16BIT_Y)
    {
        maxData = 65535;
        // sixteen bit RGB values
        float fFactor = (float)(maxData + 1);   // the +1 means 32768 maps to 0.5
        for (int rowcol = 0; rowcol < rowCount*colCount; rowcol++)
        {
            usY = ((unsigned short)input[0] << 8) + ((unsigned short)input[1]);

            fp[0] = (float)usY / fFactor;
            fp[1] = (float)usY / fFactor;
            fp[2] = (float)usY / fFactor;

            fp += 3;
            input += 2;
        }
    }
    else if (imageFormat == IMAGE_FORMAT_10BIT_RGB_L)
    {
        maxData = 1023;
        // ten bit RGB values packed to the left
        float fFactor = (float)(maxData + 1);   // the +1 means 512 maps to 0.5
        if (byteSwap)
        {
            for (int rowcol = 0; rowcol < rowCount*colCount; rowcol++)
            {

                usR = ((unsigned short)(input[3] & 0xFF) << 2) + ((unsigned short)(input[2] & 0xC0) >> 6);
                usG = ((unsigned short)(input[2] & 0x3F) << 4) + ((unsigned short)(input[1] & 0xF0) >> 4);
                usB = ((unsigned short)(input[1] & 0x0F) << 6) + ((unsigned short)(input[0] & 0xFC) >> 2);

                fp[0] = (float)usR / fFactor;
                fp[1] = (float)usG / fFactor;
                fp[2] = (float)usB / fFactor;

                fp += 3;
                input += 4;
            }
        }
        else
        {
            for (int rowcol = 0; rowcol < rowCount*colCount; rowcol++)
            {
                usR = ((unsigned short)(input[0] & 0xFF) << 2) + ((unsigned short)(input[1] & 0xC0) >> 6);
                usG = ((unsigned short)(input[1] & 0x3F) << 4) + ((unsigned short)(input[2] & 0xF0) >> 4);
                usB = ((unsigned short)(input[2] & 0x0F) << 6) + ((unsigned short)(input[3] & 0xFC) >> 2);

                fp[0] = (float)usR / fFactor;
                fp[1] = (float)usG / fFactor;
                fp[2] = (float)usB / fFactor;

                fp += 3;
                input += 4;
            }
        }
    }
    else if (imageFormat == IMAGE_FORMAT_16BIT_RGB)
    {
        maxData = 65535;
        // sixteen bit RGB values
        float fFactor = (float)(maxData + 1);   // the +1 means 32768 maps to 0.5
        for (int rowcol = 0; rowcol < rowCount*colCount; rowcol++)
        {
            usR = ((unsigned short)input[0] << 8) + ((unsigned short)input[1]);
            usG = ((unsigned short)input[2] << 8) + ((unsigned short)input[3]);
            usB = ((unsigned short)input[4] << 8) + ((unsigned short)input[5]);

            fp[0] = (float)usR / fFactor;
            fp[1] = (float)usG / fFactor;
            fp[2] = (float)usB / fFactor;

            fp += 3;
            input += 6;
        }
    }
    else if (imageFormat == IMAGE_FORMAT_10BIT_Y_L)
    {
        maxData = 1023;
        // ten bit Y values packed to the left
        float fFactor = (float)(maxData + 1);   // the +1 means 512 maps to 0.5
        if (byteSwap)
        {
            int rowcol = 0;
            for (int rowcol = 0; rowcol < rowCount*colCount; rowcol += 3)
            {
                usaY[0] = ((unsigned short)(input[3] & 0xFF) << 2) + ((unsigned short)(input[2] & 0xC0) >> 6);
                usaY[1] = ((unsigned short)(input[2] & 0x3F) << 4) + ((unsigned short)(input[1] & 0xF0) >> 4);
                usaY[2] = ((unsigned short)(input[1] & 0x0F) << 6) + ((unsigned short)(input[0] & 0xFC) >> 2);
                input += 4;

                int i = 0;
                while ((i < 3) && (rowcol + i < rowCount*colCount))
                {
                    fp[0] = (float)usaY[i] / fFactor;
                    fp[1] = (float)usaY[i] / fFactor;
                    fp[2] = (float)usaY[i] / fFactor;

                    fp += 3;
                    i++;
                }
            }
        }
        else
        {
            for (int rowcol = 0; rowcol < rowCount*colCount; rowcol += 3)
            {
                usaY[2] = ((unsigned short)(input[0] & 0xFF) << 2) + ((unsigned short)(input[1] & 0xC0) >> 6);
                usaY[1] = ((unsigned short)(input[1] & 0x3F) << 4) + ((unsigned short)(input[2] & 0xF0) >> 4);
                usaY[0] = ((unsigned short)(input[2] & 0x0F) << 6) + ((unsigned short)(input[3] & 0xFC) >> 2);
                input += 4;

                int i = 0;
                while ((i < 3) && (rowcol + i < rowCount*colCount))
                {
                    fp[0] = (float)usaY[i] / fFactor;
                    fp[1] = (float)usaY[i] / fFactor;
                    fp[2] = (float)usaY[i] / fFactor;

                    fp += 3;
                    i++;
                }
            }
        }
    }
    else if (imageFormat == IMAGE_FORMAT_16BIT_RGBA)
    {
        maxData = 65535;
        // sixteen bit RGB values
        float fFactor = (float)(maxData + 1);   // the +1 means 32768 maps to 0.5
        unsigned short usA;
        unsigned char *ucp = ucpAlpha;
        for (int rowcol = 0; rowcol < rowCount*colCount; rowcol++)
        {
            usR = ((unsigned short)input[0] << 8) + ((unsigned short)input[1]);
            usG = ((unsigned short)input[2] << 8) + ((unsigned short)input[3]);
            usB = ((unsigned short)input[4] << 8) + ((unsigned short)input[5]);

            fp[0] = (float)usR / fFactor;
            fp[1] = (float)usG / fFactor;
            fp[2] = (float)usB / fFactor;

            // the alpha channel value
            usA = ((unsigned short)input[6] << 8) + ((unsigned short)input[7]);
            ucp[0] = (usA > 0);

            fp += 3;
            ucp++;
            input += 8;
        }
    }
    else
    {
        return -1;
    }

    return 0;
}

bool CFileIO::getByteSwap(unsigned char *header)
{
    bool byteSwap = false;
    if (
        header[0] == 'X' && header[1] == 'P' && header[2] == 'D' && header[3] == 'S'
        )
    {
        byteSwap = true;
    }

    return byteSwap;

}

int CFileIO::getHeaderSize(unsigned char *header)
{
    // the header size value is the four bytes:  4, 5, 6, 7

    unsigned char *ucp = header + 4;
    int size;
    if (getByteSwap(header))
    {
        size = ((int)ucp[3] << 24) + ((int)ucp[2] << 16) + ((int)ucp[1] << 8) + (int)ucp[0];
    }
    else
    {
        size = ((int)ucp[0] << 24) + ((int)ucp[1] << 16) + ((int)ucp[2] << 8) + (int)ucp[3];
    }

    return size;
}

int CFileIO::getFileSize(unsigned char *header)
{
    // the file size value is the four bytes:  16, 17, 18, 19

    unsigned char *ucp = header + 16;
    int size;
    if (getByteSwap(header))
    {
        size = ((int)ucp[3] << 24) + ((int)ucp[2] << 16) + ((int)ucp[1] << 8) + (int)ucp[0];
    }
    else
    {
        size = ((int)ucp[0] << 24) + ((int)ucp[1] << 16) + ((int)ucp[2] << 8) + (int)ucp[3];
    }

    return size;
}


int CFileIO::getImageFormat(unsigned char *header)
{
    unsigned char *ucp;

    // the descriptor value is the one byte: 800
    ucp = header + 800;
    int descriptor = (int)ucp[0];

    // the bit size value is the one byte:  803
    ucp = header + 803;
    int bits = (int)ucp[0];

    if (bits == 10)
    {
        if (descriptor == 6)
        {
            return IMAGE_FORMAT_10BIT_Y_L;
        }
        else if (descriptor == 50)
        {
            return IMAGE_FORMAT_10BIT_RGB_L;
        }
    }
    else if (bits == 16)
    {
        if (descriptor == 0)
        {
            return IMAGE_FORMAT_16BIT_Y;
        }
        else if (descriptor == 50)
        {
            return IMAGE_FORMAT_16BIT_RGB;
        }
        else if (descriptor == 51)
        {
            return IMAGE_FORMAT_16BIT_RGBA;
        }
    }

    return IMAGE_FORMAT_NONE;
}

int CFileIO::getColCount(unsigned char *header)
{
    // the col value is the four bytes:  772, 773, 774, 775

    unsigned char *ucp = header + 772;
    int col;
    if (getByteSwap(header))
    {
        col = ((int)ucp[3] << 24) + ((int)ucp[2] << 16) + ((int)ucp[1] << 8) + (int)ucp[0];
    }
    else
    {
        col = ((int)ucp[0] << 24) + ((int)ucp[1] << 16) + ((int)ucp[2] << 8) + (int)ucp[3];
    }

    return col;
}

int CFileIO::getRowCount(unsigned char *header)
{
    // the row value is the four bytes:  776, 777, 778, 779

    unsigned char *ucp = header + 776;
    int row;

    if (getByteSwap(header))
    {
        row = ((int)ucp[3] << 24) + ((int)ucp[2] << 16) + ((int)ucp[1] << 8) + (int)ucp[0];
    }
    else
    {
        row = ((int)ucp[0] << 24) + ((int)ucp[1] << 16) + ((int)ucp[2] << 8) + (int)ucp[3];
    }

    return row;
}
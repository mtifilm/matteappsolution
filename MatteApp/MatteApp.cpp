// MatteApp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <Windows.h>
#include <cuda_runtime.h>

#include "MatteDLL.h"
#include "FileIO.h"

#define MEASURE_TIME

#ifdef MEASURE_TIME
#include <sysinfoapi.h>
#endif

#define CUDA

static void show_usage()
{
    std::cerr << "Usage: " << "MatteApp" << " <options> \n"
        << "\t-h\t\tShow this help message\n"
        << "\t-i\t\tShot number\n"
        << std::endl;
}

int main(int argc, char *argv[])
{
    std::string src;
    int frame0 = -1;
    int frame1 = -1;
    int shot = -1;

    int i = 1;
    while (i < argc)
    {
        std::string arg = argv[i++];
        if (arg == "-i")
        {
            shot = strtol(argv[i++], nullptr, 0);
        }
        else
        {
            show_usage();
            return -1;
        }
    }

    if (shot == -1)
    {
        show_usage();
        return -1;
    }

    if (shot == 0)
    {
        ////src = "J:\\media\\bars_zoom_out\\bars.%.6d.dpx";
        ////frame0 = 86400;
        ////frame1 = 86400;

        ////// Malcolm full-frame
        ////src = "J:\\media\\malcolm\\zoomed_in\\malcolm_204_cadence_fix.%.6d.dpx";
        ////frame0 = 86420;
        ////frame1 = 86420;

        // Malcolm matte
        src = "J:\\media\\blanking\\malcolm_204_cadence_fix.%.6d.dpx";
        frame0 = 86472;
        frame1 = 86472;
    }

    // Read the first frame to get sizing information
    int ret;
    CFileIO file;

    ret = file.read(src, frame0);
    if (ret)
    {
        return ret;
    }

    CMatteDLL matte;

    std::cout << "Calling setting size\n";

    // tell matte the image size
    ret = matte.setSize(file.getRow(), file.getCol(), file.getCol());
    if (ret)
    {
        return ret;
    }

#ifdef CUDA
    cudaError err = cudaSetDevice(0);
    if (err)
    {
        return err;
    }

    float *cudaBuffer;
    err = cudaMalloc(&cudaBuffer, file.getRow() * file.getCol() * 3 * sizeof(float));
    if (err)
    {
        return err;
    }

#endif
    // read all frames and pass to the library
    DWORD timeTotal = 0;
    for (int frame = frame0; frame <= frame1; frame++)
    {
        std::cout << "Calling setFrame frame: " << frame << "\n";
        ret = file.read(src, frame);
        if (ret)
        {
            return ret;
        }

#ifdef CUDA
        err = cudaMemcpy(cudaBuffer, file.getRGB(), file.getRow() * file.getCol() * 3 * sizeof(float), cudaMemcpyHostToDevice);
        if (err)
        {
            return err;
        }
#endif

        DWORD time1 = GetTickCount();

#ifdef CUDA
        ret = matte.setFrameCuda(cudaBuffer, 3);
#else
        ret = matte.setFrame(file.getRGB());
#endif

        if (ret)
        {
            return ret;
        }

        DWORD time2 = GetTickCount();

        timeTotal += (time2 - time1);
    }

#ifdef CUDA
    err = cudaFree(cudaBuffer);
    if (err)
    {
        return err;
    }
#endif

    std::cout << "Per frame processing time: " << (float)timeTotal / (float)(frame1 - frame0 + 1) << " ms\n";

    // get the result
    SMatteResult result;
    for (int i = 0; i < MATTE_SIDE_COUNT; i++)
    {
        ret = matte.getResult(-1, i, result);
        if (ret)
        {
            return ret;
        }

        printf("Side: %d start: %d  Intensity: %f  %f  %f   Grain: %f  %f  %f\n", 
            i,  result.iPictureStart,
            result.faIntensity[0], result.faIntensity[1], result.faIntensity[2], result.faGrain[0], result.faGrain[1], result.faGrain[2]);
    }
}

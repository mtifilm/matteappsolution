#ifndef MATTE_RESULT_H
#define MATTE_RESULT_H

struct SMatteResult {
	// Sometimes the matte and active picture area will be indistinguishable.
	// This can happen, for example, on a black frame with a black matte.
	// In this case the value iPictureStart will be set to MATTE_NO_PICTURE_START.
	int iPictureStart;

	float faIntensity[MATTE_NUM_CHANNEL];
	float faGrain[MATTE_NUM_CHANNEL];
};

#endif

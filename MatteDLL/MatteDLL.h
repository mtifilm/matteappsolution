#ifndef MATTE_DLL_H
#define MATTE_DLL_H

// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the MATTE_DLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// MATTE_DLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef MATTE_DLL_EXPORTS
#define MATTE_DLL_API __declspec(dllexport)
#else
#define MATTE_DLL_API __declspec(dllimport)
#endif

#include "MatteParameter.h"
#include "MatteResult.h"

// This class is exported from the dll
class MATTE_DLL_API CMatteDLL {
public:
    CMatteDLL(void);
    ~CMatteDLL(void);

    // call setSize() once to configure the CMatteDLL processing engine
    int setSize(int row, int col, int pitch);
    //  row is the number of vertical pixels in the image
    //
    //  col is the number of horizontal pixels in the image
    //
    //  pitch is the number of pixels from one row to the next

// call setFrame() once for every frame
    int setFrame(float *rgb);
    //
    //  rgb contains the image data to process
    //

    int setFrameCuda(float *rgb, int channelCount);

// call getResult() to find matte discrepancies
    int getResult(int index, int side, SMatteResult &result);
    //
    // index is the frame of interest.  Use -1 for a global result
    // side is MATTE_SIDE_TOP, MATTE_SIDE_BOTTOM, MATTE_SIDE_LEFT, or MATTE_SIDE_RIGHT
    // result is the resulting value

private:
    class CMatte *matte;
};
#endif
#ifndef MATTE_PARAMETER_H
#define MATTE_PARAMETER_H

#define MATTE_SIDE_TOP 0
#define MATTE_SIDE_BOTTOM 1
#define MATTE_SIDE_LEFT 2
#define MATTE_SIDE_RIGHT 3
#define MATTE_SIDE_COUNT 4

// Sometimes the matte and active picture area will be indistinguishable.
// This can happen, for example, on a black frame with a black matte.
// In this case the value iPictureStart will be set to MATTE_NO_PICTURE_START.
#define MATTE_NO_PICTURE_START -1

#define MATTE_NUM_CHANNEL 3

#define MATTE_INTENSITY_TOL 0.01f
#define MATTE_GRAIN_TOL 0.01f

#define MATTE_KERNEL_DIM 49
#define CUDA_BLOCK_SIZE  32
#define CUDA_MAX_BLOCK_SIZE 64
#define CUDA_FILTER_BLOCK_SIZE 128
#endif


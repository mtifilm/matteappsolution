#ifndef MATTE_EDGE_H
#define MATTE_EDGE_H

#include "MatteResult.h"
#include "ipp.h"
#include <nppdefs.h>
#include <cuda_runtime.h>

#define MATTE_VERTICAL_SEARCH .2f
#define MATTE_HORIZONTAL_SEARCH .2f
#define MATTE_VERTICAL_IGNORE .05f
#define MATTE_HORIZONTAL_IGNORE .05f

#define MATTE_PICTURE_START 0.05f

#define MATTE_THRESH_AVE 0.03f
#define MATTE_THRESH_VAR 0.0003f

#define MATTE_THRESH_MEAN 0.1f
#define MATTE_THRESH_STD 0.01f

class CMatteEdge {
public:
    CMatteEdge(void);
    ~CMatteEdge(void);

    void freeInstance();
    int setSize(int argSide, int argRow, int argCol, int argPitch);
    int setFrame(float *argRGB);
    int setFrameCuda(float *argRGB, int channelCount);
    int process();
    int processCuda(float *argRGB, int channelCount);
    int getResult(SMatteResult &resultArg);

private:
    int nRow;
    int nCol;
    int nPitch;
    int iSide;
    int iMaxGridCount;

    int iRow0;
    int iRow1;
    int iCol0;
    int iCol1;

    IppiSize sizeKern;
    IppiSize sizeSrc;
    IppiSize sizeConv;
    IppiSize sizeConvCuda;

    Ipp32f *src[MATTE_NUM_CHANNEL];
    float3 *srcCuda;
    
    Ipp32f *maxMean;
    Ipp32f *meanR;
    Ipp32f *meanG;
    Ipp32f *meanB;
    Ipp32f *maxStd;

    float *gImageResult[MATTE_NUM_CHANNEL];
    float *hImageResult[MATTE_NUM_CHANNEL];

    int nProfile;
    int iPictureStart;
    float *profile;

    SMatteResult result;

    IppEnum ConvCfg;

    void init();
};


extern "C"
{
    int gpCopyArea(
        void *source,
        float3 *target,
        int channelCount,
        int sourceColumnStart,
        int sourceRowStart,
        int sourceWidth,
        int sourceHeight,
        int targetWidth,
        int targetHeight);

    int gpMean(
        void *sourceBuffer,
        int sourceWidth,
        int sourceHeight,
        int channelCount,
        int row0,
        int row1,
        int col0,
        int col1,
        float *meanR,
        float *meanG,
        float *meanB);

    int gpStdDeviation(
        void *sourceBuffer,
        int sourceWidth,
        int sourceHeight,
        int channelCount,
        int row0,
        int row1,
        int col0,
        int col1,
        float meanR,
        float meanG,
        float meanB,
        float *stdDeviationR,
        float *stdDeviationG,
        float *stdDeviationB);
}
#endif


﻿//
//  MatteCuda.cu
//

#include <assert.h>
#include <iostream>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_fp16.h>
#include <device_atomic_functions.h>

#include "MatteRets.h"
#include "MatteParameter.h"

#define CALL(call) do{int iRet = (int)call; if(iRet != 0){printf("%s (%d) error: %d\n", __FILE__, __LINE__, iRet); return iRet;}} while (0)

// Division, rounding up to next higher integer
#define iDivUp(a, b) (((a)%(b)!= 0)?((a)/(b)+1):((a)/(b)))

extern __shared__ float shared[];

//-------------------------------------------------------------------
__device__ __inline__ int ClippedSourceIndex(int srcX, int srcY, int width, int height)
{
    int x = min(max(0, srcX), width - 1);
    int y = min(max(0, srcY), height - 1);
    return y * width + x;
}

//-----------------------------------------------------------------
__device__ __inline__ float4 PixelToFloat(float4 p4)
{
    return p4;
}

//-----------------------------------------------------------------
__device__ __inline__ float4 PixelToFloat(float3 p3)
{
    return make_float4(p3.x, p3.y, p3.z, 0);
}

//-----------------------------------------------------------------
template <typename T>
__global__ void CopyArea(
    T *source,
    float3 *target,
    int sourceColumnStart,
    int sourceRowStart,
    int sourceWidth,
    int sourceHeight,
    int targetWidth,
    int targetHeight)
{
    int iX = threadIdx.x + blockIdx.x * blockDim.x;
    int iY = threadIdx.y + blockIdx.y * blockDim.y;
    if (iX >= targetWidth || iY >= targetHeight)
    {
        return;
    }

    int idxSrc = (iY + sourceRowStart) * sourceWidth + iX + sourceColumnStart;
    assert(idxSrc < sourceWidth * sourceHeight);

    int idxTarget = iX + iY * targetWidth;
    assert(idxTarget < targetWidth * targetHeight);
    float4 f4 = PixelToFloat(source[idxSrc]);
    target[idxTarget] = make_float3(f4.x, f4.y, f4.z);
}

//-----------------------------------------------------------------
template <typename T>
__global__ void Mean(T *source, int sourceWidth, int sourceHeight, int row0, int row1, int col0, int col1, float *meanR, float *meanG, float *meanB)
{
    int sharedWidth = blockDim.x;
    int sharedHeight = blockDim.y;

    float *sharedR = shared;
    float *sharedG = sharedR + sharedWidth * sharedHeight;
    float *sharedB = sharedG + sharedWidth * sharedHeight;

    int iX = threadIdx.x + blockIdx.x * blockDim.x;
    int iY = threadIdx.y + blockIdx.y * blockDim.y;

    int srcX = col0 + iX;
    int srcY = row0 + iY;

    int tid = threadIdx.x + threadIdx.y * blockDim.x;
    int gridIdx = blockIdx.x + blockIdx.y * gridDim.x;

    if (iX < (col1 - col0) && iY < (row1 - row0))
    {
        float4 f4 = PixelToFloat(source[srcY * sourceWidth + srcX]);
        sharedR[tid] = f4.x;
        sharedG[tid] = f4.y;
        sharedB[tid] = f4.z;
    }
    else
    {
        sharedR[tid] = 0;
        sharedG[tid] = 0;
        sharedB[tid] = 0;
    }

    __syncthreads();

    for (unsigned int s = (blockDim.x * blockDim.y) / 2; s > 32; s >>= 1)
    {
        if (tid < s)
        {
            sharedR[tid] += sharedR[tid + s];
            sharedG[tid] += sharedG[tid + s];
            sharedB[tid] += sharedB[tid + s];
        }

        __syncthreads();
    }

    if (tid < 32)
    {
        sharedR[tid] += sharedR[tid + 32];
        sharedG[tid] += sharedG[tid + 32];
        sharedB[tid] += sharedB[tid + 32];

        sharedR[tid] += sharedR[tid + 16];
        sharedG[tid] += sharedG[tid + 16];
        sharedB[tid] += sharedB[tid + 16];

        sharedR[tid] += sharedR[tid + 8];
        sharedG[tid] += sharedG[tid + 8];
        sharedB[tid] += sharedB[tid + 8];

        sharedR[tid] += sharedR[tid + 4];
        sharedG[tid] += sharedG[tid + 4];
        sharedB[tid] += sharedB[tid + 4];

        sharedR[tid] += sharedR[tid + 2];
        sharedG[tid] += sharedG[tid + 2];
        sharedB[tid] += sharedB[tid + 2];

        sharedR[tid] += sharedR[tid + 1];
        sharedG[tid] += sharedG[tid + 1];
        sharedB[tid] += sharedB[tid + 1];
    }

    __syncthreads();
    if (tid == 0)
    {
        meanR[gridIdx] = sharedR[0];
        meanG[gridIdx] = sharedG[0];
        meanB[gridIdx] = sharedB[0];
    }
}

//-----------------------------------------------------------------
template <typename T>
__global__ void StandardDeviation(
    T *source,
    int sourceWidth,
    int sourceHeight,
    int row0,
    int row1,
    int col0,
    int col1,
    float meanR,
    float meanG,
    float meanB,
    float *stdDevR,
    float *stdDevG,
    float *stdDevB)
{
    int sharedWidth = blockDim.x;
    int sharedHeight = blockDim.y;

    float *sharedR = shared;
    float *sharedG = sharedR + sharedWidth * sharedHeight;
    float *sharedB = sharedG + sharedWidth * sharedHeight;

    int iX = threadIdx.x + blockIdx.x * blockDim.x;
    int iY = threadIdx.y + blockIdx.y * blockDim.y;

    int srcX = col0 + iX;
    int srcY = row0 + iY;

    int tid = threadIdx.x + threadIdx.y * blockDim.x;
    int gridIdx = blockIdx.x + blockIdx.y * gridDim.x;

    if (iX < (col1 - col0) && iY < (row1 - row0))
    {
        float4 f4 = PixelToFloat(source[srcY * sourceWidth + srcX]);
        float f = f4.x - meanR;
        sharedR[tid] = f * f;
        f = f4.y - meanG;
        sharedG[tid] = f * f;
        f = f4.z - meanB;
        sharedB[tid] = f * f;
    }
    else
    {
        sharedR[tid] = 0;
        sharedG[tid] = 0;
        sharedB[tid] = 0;
    }

    __syncthreads();

    for (unsigned int s = (blockDim.x * blockDim.y) / 2; s > 32; s >>= 1)
    {
        if (tid < s)
        {
            sharedR[tid] += sharedR[tid + s];
            sharedG[tid] += sharedG[tid + s];
            sharedB[tid] += sharedB[tid + s];
        }

        __syncthreads();
    }

    if (tid < 32)
    {
        sharedR[tid] += sharedR[tid + 32];
        sharedG[tid] += sharedG[tid + 32];
        sharedB[tid] += sharedB[tid + 32];

        sharedR[tid] += sharedR[tid + 16];
        sharedG[tid] += sharedG[tid + 16];
        sharedB[tid] += sharedB[tid + 16];

        sharedR[tid] += sharedR[tid + 8];
        sharedG[tid] += sharedG[tid + 8];
        sharedB[tid] += sharedB[tid + 8];

        sharedR[tid] += sharedR[tid + 4];
        sharedG[tid] += sharedG[tid + 4];
        sharedB[tid] += sharedB[tid + 4];

        sharedR[tid] += sharedR[tid + 2];
        sharedG[tid] += sharedG[tid + 2];
        sharedB[tid] += sharedB[tid + 2];

        sharedR[tid] += sharedR[tid + 1];
        sharedG[tid] += sharedG[tid + 1];
        sharedB[tid] += sharedB[tid + 1];
    }

    __syncthreads();
    if (tid == 0)
    {
        stdDevR[gridIdx] = sharedR[0];
        stdDevG[gridIdx] = sharedG[0];
        stdDevB[gridIdx] = sharedB[0];
    }
}

//-----------------------------------------------------------------
extern "C"
int gpMean(
    void *sourceBuffer,
    int sourceWidth,
    int sourceHeight,
    int channelCount,
    int row0,
    int row1,
    int col0,
    int col1,
    float *meanR,
    float *meanG,
    float *meanB)
{
    int width = col1 - col0;
    int height = row1 - row0;

    dim3 threadBlock(CUDA_BLOCK_SIZE, CUDA_BLOCK_SIZE, 1);
    dim3 blockGrid(1, 1, 1);
    blockGrid.x = iDivUp(width, threadBlock.x);
    blockGrid.y = iDivUp(height, threadBlock.y);

    int sharedMemorySize = threadBlock.x * threadBlock.y * 3 * sizeof(float);

    switch (channelCount)
    {
    case 3:
        Mean << < blockGrid, threadBlock, sharedMemorySize, 0 >> > ((float3*)sourceBuffer,
            sourceWidth, sourceHeight, row0, row1, col0, col1, meanR, meanG, meanB);
        break;

    case 4:
        Mean << < blockGrid, threadBlock, sharedMemorySize, 0 >> > ((float4*)sourceBuffer,
            sourceWidth, sourceHeight, row0, row1, col0, col1, meanR, meanG, meanB);
        break;

        default:
        return MATTE_RET_FAILURE;
    }

    return MATTE_RET_SUCCESS;
}

//-----------------------------------------------------------------
extern "C"
int gpCopyArea(
    void *source,
    float3 *target,
    int channelCount,
    int sourceColumnStart,
    int sourceRowStart,
    int sourceWidth,
    int sourceHeight,
    int targetWidth,
    int targetHeight)
{
    dim3 threadBlock(CUDA_BLOCK_SIZE, CUDA_BLOCK_SIZE, 1);
    dim3 blockGrid(1, 1, 1);
    blockGrid.x = iDivUp(targetWidth, threadBlock.x);
    blockGrid.y = iDivUp(targetHeight, threadBlock.y);

    switch (channelCount)
    {
    case 3:
        CopyArea << < blockGrid, threadBlock, 0, 0 >> > (
            (float3*)source,
            target,
            sourceColumnStart,
            sourceRowStart,
            sourceWidth,
            sourceHeight,
            targetWidth,
            targetHeight);
        break;

    case 4:
        CopyArea << < blockGrid, threadBlock, 0, 0 >> > (
            (float4*)source,
            target,
            sourceColumnStart,
            sourceRowStart,
            sourceWidth,
            sourceHeight,
            targetWidth,
            targetHeight);
        break;

    default:
        return MATTE_RET_FAILURE;
    }

    return MATTE_RET_SUCCESS;
}
//-----------------------------------------------------------------
extern "C"
int gpStdDeviation(
    void *sourceBuffer,
    int sourceWidth,
    int sourceHeight,
    int channelCount,
    int row0,
    int row1,
    int col0,
    int col1,
    float meanR,
    float meanG,
    float meanB,
    float *stdDeviationR,
    float *stdDeviationG,
    float *stdDeviationB)
{
    int width = col1 - col0;
    int height = row1 - row0;

    dim3 threadBlock(CUDA_BLOCK_SIZE, CUDA_BLOCK_SIZE, 1);
    dim3 blockGrid(1, 1, 1);
    blockGrid.x = iDivUp(width, threadBlock.x);
    blockGrid.y = iDivUp(height, threadBlock.y);

    int sharedMemorySize = threadBlock.x * threadBlock.y * 3 * sizeof(float);

    switch (channelCount)
    {
    case 3:
        StandardDeviation << < blockGrid, threadBlock, sharedMemorySize, 0 >> > ((float3*)sourceBuffer,
            sourceWidth, sourceHeight, row0, row1, col0, col1,
            meanR, meanG, meanB, stdDeviationR, stdDeviationG, stdDeviationB);
        break;

    case 4:
        StandardDeviation << < blockGrid, threadBlock, sharedMemorySize, 0 >> > ((float4*)sourceBuffer,
            sourceWidth, sourceHeight, row0, row1, col0, col1,
            meanR, meanG, meanB, stdDeviationR, stdDeviationG, stdDeviationB);
        break;

    default:
        return MATTE_RET_FAILURE;
    }

    return MATTE_RET_SUCCESS;
}
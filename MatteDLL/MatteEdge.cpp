#include "pch.h"
#include <math.h>
#include <npp.h>
#include <nppi.h>
#include <chrono>
#include <iostream>

#include "MatteParameter.h"
#include "MatteEdge.h"
#include "MatteRets.h"

using namespace std;
using chrono::duration_cast;
using chrono::microseconds;

// Division, rounding up to next higher integer
#define iDivUp(a, b) (((a)%(b)!= 0)?((a)/(b)+1):((a)/(b)))

CMatteEdge::CMatteEdge()
{
    for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
    {
        src[i] = nullptr;
        gImageResult[i] = nullptr;
        hImageResult[i] = nullptr;
    }

    srcCuda = nullptr;
    profile = nullptr;

    iMaxGridCount = 0;

    maxMean = nullptr;
    meanR = nullptr;
    meanG = nullptr;
    meanB = nullptr;
    maxStd = nullptr;
    init();
}

CMatteEdge::~CMatteEdge()
{
    freeInstance();
}

void CMatteEdge::init()
{
    nRow = -1;
    nCol = -1;
    nPitch = -1;
    iSide = -1;
    nProfile = -1;
}

void CMatteEdge::freeInstance()
{
    for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
    {
        ippFree(src[i]);
        src[i] = nullptr;

        if (gImageResult[i] != nullptr)
        {
            cudaFree(gImageResult[i]);
            gImageResult[i] = nullptr;
        }

        free(hImageResult[i]);
        hImageResult[i] = nullptr;
    }

    cudaFree(srcCuda);
    srcCuda = nullptr;

    delete[] profile;
    profile = nullptr;

    ippFree(maxMean);
    maxMean = nullptr;

    ippFree(meanR);
    meanR = nullptr;

    ippFree(meanG);
    meanG = nullptr;

    ippFree(meanB);
    meanB = nullptr;

    ippFree(maxStd);
    maxStd = nullptr;

    init();
}

int CMatteEdge::setSize(int argSide, int argRow, int argCol, int argPitch)
{
    cudaError err;
    freeInstance();

    if (argSide == MATTE_SIDE_TOP)
    {
        iRow0 = 0;
        iRow1 = (int)roundf(MATTE_VERTICAL_SEARCH * (float)argRow);
        iCol0 = (int)roundf(MATTE_HORIZONTAL_IGNORE * (float)argCol);
        iCol1 = (int)round((1.f - MATTE_HORIZONTAL_IGNORE) * (float)argCol);
        sizeKern.height = 1;
        sizeKern.width = MATTE_KERNEL_DIM;
    }
    else if (argSide == MATTE_SIDE_BOTTOM)
    {
        iRow0 = (int)roundf((1.f - MATTE_VERTICAL_SEARCH) * (float)argRow);
        iRow1 = argRow;
        iCol0 = (int)roundf(MATTE_HORIZONTAL_IGNORE * (float)argCol);
        iCol1 = (int)round((1.f - MATTE_HORIZONTAL_IGNORE) * (float)argCol);
        sizeKern.height = 1;
        sizeKern.width = MATTE_KERNEL_DIM;
    }
    else if (argSide == MATTE_SIDE_LEFT)
    {

        iRow0 = (int)roundf(MATTE_VERTICAL_IGNORE * (float)argRow);
        iRow1 = (int)round((1.f - MATTE_VERTICAL_IGNORE) * (float)argRow);
        iCol0 = 0;
        iCol1 = (int)roundf(MATTE_HORIZONTAL_SEARCH * (float)argCol);
        sizeKern.height = MATTE_KERNEL_DIM;
        sizeKern.width = 1;
    }
    else if (argSide == MATTE_SIDE_RIGHT)
    {
        iRow0 = (int)roundf(MATTE_VERTICAL_IGNORE * (float)argRow);
        iRow1 = (int)round((1.f - MATTE_VERTICAL_IGNORE) * (float)argRow);
        iCol0 = (int)roundf((1.f - MATTE_HORIZONTAL_SEARCH) * (float)argCol);
        iCol1 = argCol;
        sizeKern.height = MATTE_KERNEL_DIM;
        sizeKern.width = 1;
    }

    sizeSrc.height = iRow1 - iRow0;
    sizeSrc.width = iCol1 - iCol0;
    sizeConv.height = sizeSrc.height + sizeKern.height - 1;
    sizeConv.width = sizeSrc.width + sizeKern.width - 1;
    sizeConvCuda.width = sizeSrc.width;
    sizeConvCuda.height = sizeSrc.height;

    for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
    {
        src[i] = (Ipp32f *)ippMalloc(sizeSrc.height * sizeSrc.width * sizeof(float));
        if (src[i] == nullptr)
        {
            return MATTE_ERR_ALLOC;
        }
    }

    err = cudaMalloc(&srcCuda, sizeSrc.height * sizeSrc.width * sizeof(float3));
    if (err)
    {
        return MATTE_ERR_ALLOC;
    }

    int gridWidth, gridHeight;
    if (argSide == MATTE_SIDE_TOP || argSide == MATTE_SIDE_BOTTOM)
    {
        gridWidth = iDivUp(sizeSrc.width, CUDA_MAX_BLOCK_SIZE);
        gridHeight = sizeSrc.height;
    }
    else
    {
        gridWidth = sizeSrc.width;
        gridHeight = iDivUp(sizeSrc.height, CUDA_MAX_BLOCK_SIZE);
    }

    // set up the buffer for convolution
    IppStatus status;
    ConvCfg = ippAlgAuto | ippiROIFull;
    if (argSide == MATTE_SIDE_TOP || argSide == MATTE_SIDE_BOTTOM)
    {
        nProfile = sizeSrc.height;
    }
    else
    {
        nProfile = sizeSrc.width;
    }

    profile = new float[nProfile];

    maxMean = (Ipp32f *)ippMalloc(nProfile * sizeof(float));
    meanR= (Ipp32f *)ippMalloc(nProfile * sizeof(float));
    meanG = (Ipp32f *)ippMalloc(nProfile * sizeof(float));
    meanB = (Ipp32f *)ippMalloc(nProfile * sizeof(float));
    maxStd = (Ipp32f *)ippMalloc(nProfile * sizeof(float));
    if (maxMean == nullptr || maxStd == nullptr)
    {
        return MATTE_ERR_ALLOC;
    }

    iSide = argSide;
    nRow = argRow;
    nCol = argCol;
    nPitch = argPitch;

    return MATTE_RET_SUCCESS;
}

int CMatteEdge::setFrame(float *argRGB)
{
    IppStatus status;

    // initialize the values of maxMean and maxStd
    for (int j = 0; j < nProfile; j++)
    {
        maxMean[j] = 0.f;
        maxStd[j] = 0.f;
    }

    // analyze each channel separately
    for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
    {
        // copy out of the RGB frame buffer into source
        status = ippiCopy_32f_C3C1R(argRGB + (iRow0 * nCol + iCol0)*MATTE_NUM_CHANNEL + i, 4 * nPitch * 3, src[i], 4 * sizeSrc.width, sizeSrc);
        if (status)
        {
            return MATTE_ERR_COPY;
        }

        // for each line in the profile, find the mean and std
        if (iSide == MATTE_SIDE_TOP || iSide == MATTE_SIDE_BOTTOM)
        {
            IppiSize sizeLine;
            sizeLine.height = 1;
            sizeLine.width = sizeSrc.width;

            for (int j = 0; j < nProfile; j++)
            {
                Ipp64f tmpMean, tmpStd;
                status = ippiMean_StdDev_32f_C1R(src[i] + j * sizeSrc.width, 4 * sizeSrc.width, sizeLine, &tmpMean, &tmpStd);
                if (status)
                {
                    return MATTE_ERR_MAX;
                }

                maxMean[j] = max(maxMean[j], (Ipp32f)tmpMean);
                maxStd[j] = max(maxStd[j], (Ipp32f)tmpStd);
            }
        }
        else
        {
            IppiSize sizeLine;
            sizeLine.height = sizeSrc.height;
            sizeLine.width = 1;

            for (int j = 0; j < nProfile; j++)
            {
                Ipp64f tmpMean, tmpStd;
                status = ippiMean_StdDev_32f_C1R(src[i] + j, 4 * sizeSrc.width, sizeLine, &tmpMean, &tmpStd);
                if (status)
                {
                    return MATTE_ERR_MAX;
                }

                maxMean[j] = max(maxMean[j], (Ipp32f)tmpMean);
                maxStd[j] = max(maxStd[j], (Ipp32f)tmpStd);
            }
        }
    }

    return MATTE_RET_SUCCESS;
}

int CMatteEdge::setFrameCuda(float *argRGB, int channelCount)
{
    cudaError_t error;

    IppiSize sizeProfile;
    sizeProfile.height = nProfile;
    sizeProfile.width = 1;
    memset(maxMean, 0, sizeof(float) * nProfile);
    memset(maxStd, 0, sizeof(float) * nProfile);

    int gridWidth, gridHeight;
    if (iSide == MATTE_SIDE_TOP || iSide == MATTE_SIDE_BOTTOM)
    {
        gridWidth = iDivUp(iCol1 - iCol0, CUDA_BLOCK_SIZE);
        gridHeight = 1;
    }
    else
    {
        gridWidth = 1;
        gridHeight = iDivUp(iRow1 - iRow0, CUDA_BLOCK_SIZE);
    }

    int gridCount = gridWidth * gridHeight;
    if (gridCount > iMaxGridCount)
    {
        for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
        {
            if (gImageResult[i] != nullptr)
            {
                error = cudaFree(gImageResult[i]);
                if (error)
                {
                    return MATTE_RET_FAILURE;
                }
            }

            free(hImageResult[i]);

            error = cudaMalloc(&gImageResult[i], gridCount * sizeof(float));
            if (error)
            {
                return MATTE_RET_FAILURE;
            }

            hImageResult[i] = (float*)malloc(gridCount * sizeof(float));
        }

        iMaxGridCount = gridCount;
    }

    // copy the region of interest from the source image
    int ret = gpCopyArea(
        argRGB,
        srcCuda,
        channelCount,
        iCol0,
        iRow0,
        nCol,
        nRow,
        sizeSrc.width,
        sizeSrc.height);

    if (ret)
    {
        return MATTE_RET_FAILURE;
    }

    for (int i = 0; i < nProfile; i++)
    {
        if (iSide == MATTE_SIDE_TOP || iSide == MATTE_SIDE_BOTTOM)
        {
            ret = gpMean(srcCuda, sizeSrc.width, sizeSrc.height, channelCount, i, i + 1, 0, sizeSrc.width, gImageResult[0], gImageResult[1], gImageResult[2]);
        }
        else
        {
            ret = gpMean(srcCuda, sizeSrc.width, sizeSrc.height, channelCount, 0, sizeSrc.height, i, i + 1, gImageResult[0], gImageResult[1], gImageResult[2]);
        }

        if (ret)
        {
            return MATTE_RET_FAILURE;
        }

        for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
        {
            error = cudaMemcpy(hImageResult[i], gImageResult[i], gridCount * sizeof(float), cudaMemcpyDeviceToHost);
            if (error)
            {
                return MATTE_RET_FAILURE;
            }
        }

        float r = 0, g = 0, b = 0;
        for (int i = 0; i < gridCount; i++)
        {
            r += hImageResult[0][i];
            g += hImageResult[1][i];
            b += hImageResult[2][i];
        }

        int pixelCount;
        if (iSide == MATTE_SIDE_TOP || iSide == MATTE_SIDE_BOTTOM)
        {
            pixelCount = iCol1 - iCol0;
        }
        else
        {
            pixelCount = iRow1 - iRow0;
        }

        r /= pixelCount;
        g /= pixelCount;
        b /= pixelCount;

        meanR[i] = r;
        meanG[i] = g;
        meanB[i] = b;
        maxMean[i] = max(r, max(g, b));
    }

    for (int i = 0; i < nProfile; i++)
    {
        if (iSide == MATTE_SIDE_TOP || iSide == MATTE_SIDE_BOTTOM)
        {
            ret = gpStdDeviation(
                srcCuda, 
                sizeSrc.width, 
                sizeSrc.height, 
                channelCount, 
                i, 
                i + 1, 
                0, 
                sizeSrc.width,
                meanR[i],
                meanG[i],
                meanB[i],
                gImageResult[0], 
                gImageResult[1], 
                gImageResult[2]);
        }
        else
        {
            ret = gpStdDeviation(
                srcCuda, 
                sizeSrc.width, 
                sizeSrc.height, 
                channelCount, 
                0, 
                sizeSrc.height, 
                i, 
                i + 1, 
                meanR[i],
                meanG[i],
                meanB[i],
                gImageResult[0],
                gImageResult[1], 
                gImageResult[2]);
        }

        if (ret)
        {
            return MATTE_RET_FAILURE;
        }

        for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
        {
            error = cudaMemcpy(hImageResult[i], gImageResult[i], gridCount * sizeof(float), cudaMemcpyDeviceToHost);
            if (error)
            {
                return MATTE_RET_FAILURE;
            }
        }

        float r = 0, g = 0, b = 0;
        for (int i = 0; i < gridCount; i++)
        {
            r += hImageResult[0][i];
            g += hImageResult[1][i];
            b += hImageResult[2][i];
        }

        int pixelCount;
        if (iSide == MATTE_SIDE_TOP || iSide == MATTE_SIDE_BOTTOM)
        {
            pixelCount = iCol1 - iCol0;
        }
        else
        {
            pixelCount = iRow1 - iRow0;
        }

        maxStd[i] = sqrt(max(r, max(g, b)) / pixelCount);
    }

    return MATTE_RET_SUCCESS;
}

int CMatteEdge::process()
{
    IppStatus status;

    // find the location where the profile indicates start of picture
    iPictureStart = MATTE_NO_PICTURE_START;
    if (iSide == MATTE_SIDE_TOP || iSide == MATTE_SIDE_LEFT)
    {
        for (int i = 0; i < nProfile; i++)
        {
            if (maxMean[i] > MATTE_THRESH_MEAN || maxStd[i] > MATTE_THRESH_STD)
            {
                iPictureStart = i;
                i = nProfile;
            }
        }
    }
    else
    {
        for (int i = nProfile - 1; i >= 0; i--)
        {
            if (maxMean[i] > MATTE_THRESH_MEAN || maxStd[i] > MATTE_THRESH_STD)
            {
                iPictureStart = i;
                i = 0;
            }
        }
    }

    // calculate the statistics of the matte
    int row0, row1, col0, col1;
    if (iPictureStart == MATTE_NO_PICTURE_START)
    {
        // no matte was detected.  This can happen on a black frame with a black matte
        result.iPictureStart = MATTE_NO_PICTURE_START;
        row0 = 0;
        row1 = sizeSrc.height;
        col0 = 0;
        col1 = sizeSrc.width;
    }
    else
    {
        if (iSide == MATTE_SIDE_TOP)
        {
            result.iPictureStart = iRow0 + iPictureStart;
            row0 = 0;
            row1 = iPictureStart;
            col0 = 0;
            col1 = sizeSrc.width;
        }
        else if (iSide == MATTE_SIDE_BOTTOM)
        {
            result.iPictureStart = iRow0 + iPictureStart;
            row0 = iPictureStart + 1;
            row1 = sizeSrc.height;
            col0 = 0;
            col1 = sizeSrc.width;
        }
        else if (iSide == MATTE_SIDE_LEFT)
        {
            result.iPictureStart = iCol0 + iPictureStart;
            row0 = 0;
            row1 = sizeSrc.height;
            col0 = 0;
            col1 = iPictureStart;
        }
        else if (iSide == MATTE_SIDE_RIGHT)
        {
            result.iPictureStart = iCol0 + iPictureStart;
            row0 = 0;
            row1 = sizeSrc.height;
            col0 = iPictureStart + 1;
            col1 = sizeSrc.width;
        }
    }

    // calculate the mean and std deviation
    IppiSize sizeTmp;
    sizeTmp.height = row1 - row0;
    sizeTmp.width = col1 - col0;
    Ipp64f mean;
    Ipp64f std;
    for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
    {
        if (sizeTmp.height == 0 || sizeTmp.width == 0)
        {
            // the picture start is at the edge.  No matte values to calculate
            mean = -1.f;
            std = -1.f;
        }
        else
        {
            status = ippiMean_StdDev_32f_C1R(src[i] + row0 * sizeSrc.width + col0, sizeof(float) * sizeSrc.width, sizeTmp, &mean, &std);
            if (status)
            {
                return MATTE_ERR_STATS;
            }
        }

        result.faIntensity[i] = (float)mean;
        result.faGrain[i] = (float)std;
    }

    return MATTE_RET_SUCCESS;
}

int CMatteEdge::processCuda(float *argRGB, int channelCount)
{
    cudaError error;

    // find the location where the profile indicates start of picture
    iPictureStart = MATTE_NO_PICTURE_START;
    if (iSide == MATTE_SIDE_TOP || iSide == MATTE_SIDE_LEFT)
    {
        for (int i = 0; i < nProfile; i++)
        {
            if (maxMean[i] > MATTE_THRESH_MEAN || maxStd[i] > MATTE_THRESH_STD)
            {
                iPictureStart = i;
                i = nProfile;
            }
        }
    }
    else
    {
        for (int i = nProfile - 1; i >= 0; i--)
        {
            if (maxMean[i] > MATTE_THRESH_MEAN || maxStd[i] > MATTE_THRESH_STD)
            {
                iPictureStart = i;
                i = 0;
            }
        }
    }

    // calculate the statistics of the matte
    int row0, row1, col0, col1;
    if (iPictureStart == MATTE_NO_PICTURE_START)
    {
        // no matte was detected.  This can happen on a black frame with a black matte
        result.iPictureStart = MATTE_NO_PICTURE_START;
        row0 = 0;
        row1 = sizeSrc.height;
        col0 = 0;
        col1 = sizeSrc.width;
    }
    else
    {
        if (iSide == MATTE_SIDE_TOP)
        {
            result.iPictureStart = iRow0 + iPictureStart;
            row0 = 0;
            row1 = iPictureStart;
            col0 = iCol0;
            col1 = iCol0 + sizeSrc.width;
        }
        else if (iSide == MATTE_SIDE_BOTTOM)
        {
            result.iPictureStart = iRow0 + iPictureStart;
            row0 = result.iPictureStart + 1;
            row1 = iRow1;
            col0 = iCol0;
            col1 = iCol0 + sizeSrc.width;
        }
        else if (iSide == MATTE_SIDE_LEFT)
        {
            result.iPictureStart = iCol0 + iPictureStart;
            row0 = iRow0;
            row1 = iRow0 + sizeSrc.height;
            col0 = 0;
            col1 = iPictureStart;
        }
        else if (iSide == MATTE_SIDE_RIGHT)
        {
            result.iPictureStart = iCol0 + iPictureStart;
            row0 = iRow0;
            row1 = iRow0 + sizeSrc.height;
            col0 = result.iPictureStart + 1;
            col1 = nCol;
        }
    }

    if (col1 - col0 == 0 || row1 - row0 == 0)
    {
        result.faGrain[0] = -1;
        result.faGrain[1] = -1;
        result.faGrain[2] = -1;

        result.faIntensity[0] = -1;
        result.faIntensity[1] = -1;
        result.faIntensity[2] = -1;

        return MATTE_RET_SUCCESS;
    }

    // calculate the mean and std deviation
    int gridWidth = iDivUp(col1 - col0, CUDA_BLOCK_SIZE);
    int gridHeight = iDivUp(row1 - row0, CUDA_BLOCK_SIZE);
    int gridCount = gridWidth * gridHeight;
    if (gridCount > iMaxGridCount)
    {
        for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
        {
            if (gImageResult[i] != nullptr)
            {
                error = cudaFree(gImageResult[i]);
                if (error)
                {
                    return MATTE_RET_FAILURE;
                }
            }

            free(hImageResult[i]);

            error = cudaMalloc(&gImageResult[i], gridCount * sizeof(float));
            if (error)
            {
                return MATTE_RET_FAILURE;
            }

            hImageResult[i] = (float*)malloc(gridCount * sizeof(float));
        }

        iMaxGridCount = gridCount;
    }

    int ret = gpMean(argRGB, nCol, nRow, channelCount, row0, row1, col0, col1, gImageResult[0], gImageResult[1], gImageResult[2]);
    if (ret)
    {
        return MATTE_RET_FAILURE;
    }

    for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
    {
        error = cudaMemcpy(hImageResult[i], gImageResult[i], gridCount * sizeof(float), cudaMemcpyDeviceToHost);
        if (error)
        {
            return MATTE_RET_FAILURE;
        }
    }

    float r = 0, g = 0, b = 0;
    for (int i = 0; i < gridCount; i++)
    {
        r += hImageResult[0][i];
        g += hImageResult[1][i];
        b += hImageResult[2][i];
    }

    int pixelCount = (row1 - row0) * (col1 - col0);

    result.faIntensity[0] = r / pixelCount;
    result.faIntensity[1] = g / pixelCount;
    result.faIntensity[2] = b / pixelCount;

    ret = gpStdDeviation(
        argRGB,
        nCol,
        nRow,
        channelCount,
        row0,
        row1,
        col0,
        col1,
        result.faIntensity[0],
        result.faIntensity[1],
        result.faIntensity[2],
        gImageResult[0],
        gImageResult[1],
        gImageResult[2]);
    if (ret)
    {
        return MATTE_RET_FAILURE;
    }

    for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
    {
        error = cudaMemcpy(hImageResult[i], gImageResult[i], gridCount * sizeof(float), cudaMemcpyDeviceToHost);
        if (error)
        {
            return MATTE_RET_FAILURE;
        }
    }

    r = 0;
    g = 0;
    b = 0;
    for (int i = 0; i < gridCount; i++)
    {
        r += hImageResult[0][i];
        g += hImageResult[1][i];
        b += hImageResult[2][i];
    }

    result.faGrain[0] = sqrt(r / pixelCount);
    result.faGrain[1] = sqrt(g / pixelCount);
    result.faGrain[2] = sqrt(b / pixelCount);

    return MATTE_RET_SUCCESS;
}

int CMatteEdge::getResult(SMatteResult &resultArg)
{
    resultArg = result;

    return MATTE_RET_SUCCESS;
}

#include "pch.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <iostream> 

#include "Matte.h"
#include "MatteRets.h"

CMatte::CMatte()
{
    init();
}

CMatte::~CMatte()
{
    free();
}

int CMatte::setSize(int argRow, int argCol, int argPitch)
{
    // delete previous memory
    free();
    init();

    // check for errors
    if (argRow <= 0 || argCol <= 0 || argPitch < argCol)
    {
        return MATTE_ERR_IMAGE_SIZE;
    }

    int ret;
    for (int i = 0; i < MATTE_SIDE_COUNT; i++)
    {
        ret = edge[i].setSize(i, argRow, argCol, argPitch);
        if (ret)
        {
            return ret;
        }
    }

    nRow = argRow;
    nCol = argCol;
    nPitch = argPitch;

    return MATTE_RET_SUCCESS;
}

void CMatte::init()
{
    nRow = -1;
    nCol = -1;
    nPitch = -1;
    bResultAvailable = false;
}

void CMatte::free()
{
    for (int i = 0; i < MATTE_SIDE_COUNT; i++)
    {
        edge[i].freeInstance();
        listResult[i].clear();
    }
}

int CMatte::setFrame(float *argRGB)
{
    // check for errors
    if (nRow <= 0 || nCol <= 0 || nPitch < nCol)
    {
        return MATTE_ERR_IMAGE_SIZE;
    }

    bResultAvailable = false;

    // copy the data into the edges
    int ret;
    for (int i = 0; i < MATTE_SIDE_COUNT; i++)
    {
        ret = edge[i].setFrame(argRGB);
        if (ret)
        {
            return ret;
        }
    }

    // process the edges
    for (int i = 0; i < MATTE_SIDE_COUNT; i++)
    {
        ret = edge[i].process();
        if (ret)
        {
            return ret;
        }
    }

    // extract the results for this frame
    for (int i = 0; i < MATTE_SIDE_COUNT; i++)
    {
        SMatteResult resultLocal;
        ret = edge[i].getResult(resultLocal);
        if (ret)
        {
            return ret;
        }

        listResult[i].push_back(resultLocal);
    }

    return MATTE_RET_SUCCESS;
}

int CMatte::setFrameCuda(float *argRGB, int channelCount)
{
    // check for errors
    if (nRow <= 0 || nCol <= 0 || nPitch < nCol)
    {
        return MATTE_ERR_IMAGE_SIZE;
    }

    bResultAvailable = false;

    // copy the data into the edges
    int ret;
    for (int i = 0; i < MATTE_SIDE_COUNT; i++)
    {
        ret = edge[i].setFrameCuda(argRGB, channelCount);
        if (ret)
        {
            return ret;
        }

        // process the edges
        ret = edge[i].processCuda(argRGB, channelCount);
        if (ret)
        {
            return ret;
        }

        // extract the results for this frame
        SMatteResult resultLocal;
        ret = edge[i].getResult(resultLocal);
        if (ret)
        {
            return ret;
        }

        listResult[i].push_back(resultLocal);
    }

    return MATTE_RET_SUCCESS;
}

int CMatte::getResult(int argIndex, int argSide, SMatteResult &result)
{
    // check for errors
    if ((int)listResult->size() == 0)
    {
        return MATTE_ERR_EMPTY_LIST;
    }

    if (argIndex < -1 || argIndex >= (int)listResult->size())
    {
        return MATTE_ERR_INDEX;
    }

    if (argSide < 0 || argSide >= MATTE_SIDE_COUNT)
    {
        return MATTE_ERR_SIDE;
    }

    if (bResultAvailable == false)
    {
        for (int i = 0; i < MATTE_SIDE_COUNT; i++)
        {
            FindBest(i);
        }

        bResultAvailable = true;
    }

    if (argIndex == -1)
    {
        if (bResultAvailable == false)
        {
            for (int i = 0; i < MATTE_SIDE_COUNT; i++)
            {
                FindBest(i);
            }

            bResultAvailable = true;
        }

        result = resultBest[argSide];
    }
    else
    {
        result = listResult[argSide].at(argIndex);
    }

    return MATTE_RET_SUCCESS;
}

void CMatte::FindBest(int argSide)
{
    // initialize resultBest
    resultBest[argSide].iPictureStart = MATTE_NO_PICTURE_START;
    for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
    {
        resultBest[argSide].faIntensity[i] = 0.f;
        resultBest[argSide].faGrain[i] = 0.f;
    }

    // find the smallest and largest PictureStart
    int iSmallest = 1000000;
    int iLargest = 0;
    for (int i = 0; i < listResult[argSide].size(); i++)
    {
        if (listResult[argSide].at(i).iPictureStart != MATTE_NO_PICTURE_START)
        {
            iSmallest = min(iSmallest, listResult[argSide].at(i).iPictureStart);
            iLargest = max(iLargest, listResult[argSide].at(i).iPictureStart);
        }
    }

    // find the most common PpictureStart
    if (iLargest >= iSmallest)
    {
        // clear the picture start counter
        std::vector <int> listCount;
        for (int i = iSmallest; i <= iLargest; i++)
        {
            listCount.push_back(0);
        }

        // increment the picture strat counter
        for (int i = 0; i < listResult[argSide].size(); i++)
        {
            if (listResult[argSide].at(i).iPictureStart != MATTE_NO_PICTURE_START)
            {
                listCount.at(listResult[argSide].at(i).iPictureStart - iSmallest)++;
            }
        }

        // determine the most common picture start
        int iMaxCount = 0;
        for (int i = iSmallest; i <= iLargest; i++)
        {
            if (listCount.at(i - iSmallest) > iMaxCount)
            {
                iMaxCount = listCount.at(i - iSmallest);
                resultBest[argSide].iPictureStart = i;
            }
        }
    }

    // calculate the average intensity and grain values for the best picture start
    int iNSum = 0;
    for (int i = 0; i < listResult[argSide].size(); i++)
    {
        if (listResult[argSide].at(i).iPictureStart == resultBest[argSide].iPictureStart)
        {
            iNSum++;
            for (int j = 0; j < MATTE_NUM_CHANNEL; j++)
            {
                resultBest[argSide].faIntensity[j] += listResult[argSide].at(i).faIntensity[j];
                resultBest[argSide].faGrain[j] += listResult[argSide].at(i).faGrain[j];
            }
        }
    }

    for (int i = 0; i < MATTE_NUM_CHANNEL; i++)
    {
        resultBest[argSide].faIntensity[i] /= (float)iNSum;
        resultBest[argSide].faGrain[i] /= (float)iNSum;
    }

}

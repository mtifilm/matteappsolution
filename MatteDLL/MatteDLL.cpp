// MatteDLL.cpp : Defines the exported functions for the DLL.
//

#include "pch.h"

#include "MatteDLL.h"
#include "Matte.h"

CMatteDLL::CMatteDLL()
{
    matte = new CMatte;
}

CMatteDLL::~CMatteDLL()
{
    delete matte;
}

int CMatteDLL::setSize(int row, int col, int pitch)
{
    return matte->setSize(row, col, pitch);
}

int CMatteDLL::setFrame(float *rgb)
{
    return matte->setFrame(rgb);
}

int CMatteDLL::setFrameCuda(float *rgb, int channelCount)
{
    return matte->setFrameCuda(rgb, channelCount);
}

int CMatteDLL::getResult(int index, int side, SMatteResult &result)
{
    return matte->getResult(index, side, result);
}

#ifndef MATTE_H
#define MATTE_H

#include <vector>

/*
    The CMatte class analyzes a range of frames to determine the
    most common picture start of each matte (top, bottom, left, and right).
    CMatte also reports discrepancies for the mattes on each frame
    in the sequence.

    To use CMatte, first call setSize().  Then call setFrame()
    for each frame in the sequence.   Finally, call getResult()
    for each frame in the sequence to determine the discrepancies.

    getResult() can be called with argIndex = -1 to determine the
    matte values of the entire sequence.
*/

#include "MatteParameter.h"
#include "MatteResult.h"
#include "MatteEdge.h"

class CMatte {
public:
    CMatte(void);
    ~CMatte(void);

    int setSize(int argRow, int argCol, int argPitch);
    // pitch is the number of pixels from one row to the next

    int setFrame(float *argRGB);
    int setFrameCuda(float *argRGB, int channelCount);

    int getResult(int argIndex, int argSide, SMatteResult &result);

private:

    CMatteEdge edge[MATTE_SIDE_COUNT];
    std::vector <SMatteResult> listResult[MATTE_SIDE_COUNT];
    SMatteResult resultBest[MATTE_SIDE_COUNT];

    int nRow;
    int nCol;
    int nPitch;
    bool bResultAvailable;

    void init();
    void free();
    void FindBest(int argSide);
};

#endif
